/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendeprogramacion2019.BO;

import examendeprogramacion2019.DAO.RecetasDAO;
import examendeprogramacion2019.ENTITIS.Recetas;
import java.util.LinkedList;

/**
 *
 * @author Fabian
 */
public class RecetasBO {
    public LinkedList<Recetas> buscar(String buscar, boolean todos, boolean inactivos) {
        if (buscar.trim().length() < 3 && !todos) {
            throw new RuntimeException("Mínimo 3 caracteres para filtrar");
        }
        return new RecetasDAO().cargarTodo(buscar, todos, inactivos);
    }

    public void guardar(Recetas rc) {
        // validaciones respectepivas del articulo 
   
            new RecetasDAO().insertar(rc);
        
    }
}
