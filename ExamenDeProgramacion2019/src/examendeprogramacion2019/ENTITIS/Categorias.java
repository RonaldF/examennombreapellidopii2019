/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendeprogramacion2019.ENTITIS;

/**
 *
 * @author Fabian
 */
public class Categorias {
    private String categorias;
    private boolean activo;

    public Categorias() {
    }

    public Categorias(String categorias, boolean activo) {
        this.categorias = categorias;
        this.activo = activo;
    }

    public String getCategorias() {
        return categorias;
    }

    public void setCategorias(String categorias) {
        this.categorias = categorias;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Categorias{" + "categorias=" + categorias + ", activo=" + activo + '}';
    }
    
    
}
