/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendeprogramacion2019.ENTITIS;

/**
 *
 * @author Fabian
 */
public class Recetas {
    private int id;
    private int id_categoria;
    private String nombre;
    private String ingredientes;
    private String indicaciones;
    private boolean activo;

    public Recetas() {
        activo=true;
    }

    public Recetas(int id, int id_categoria, String nombre, String ingredientes, String indicaciones, boolean activo) {
        this.id= id;
        this.id_categoria = id_categoria;
        this.nombre = nombre;
        this.ingredientes = ingredientes;
        this.indicaciones = indicaciones;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Recetas{" + "id=" + id + ", id_categoria=" + id_categoria + ", nombre=" + nombre + ", ingredientes=" + ingredientes + ", indicaciones=" + indicaciones + ", activo=" + activo + '}';
    }

    
    
}
