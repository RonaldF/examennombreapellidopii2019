/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendeprogramacion2019.DAO;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Fabian
 */
public class Conexion {
    private static final String DRIVER = "org.postgresql.Driver";
    private static final String TIPO = "jdbc:postgresql://";
    private static final String SERVER = "localhost";
    private static final String PUERTO = "5432";
    private static final String DB = "examen";
    private static final String USER = "postgres";
    private static final String PASS = "fabianporras1213";
    
    public static Connection conexion() throws Exception {

        Connection c = null;
        try {
            Class.forName(DRIVER);
            String strCon = String.format("%s%s:%s/%s", TIPO, SERVER, PUERTO, DB);
            c = DriverManager.getConnection(strCon, USER, PASS);
            System.out.println("Conexion");
            return c;
        } catch (Exception ex) {
            throw new Exception("Problemas con la conexión al servidor");

        }
    }
    public static void main(String[] args) {
        try {
            conexion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
