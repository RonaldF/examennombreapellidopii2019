/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendeprogramacion2019.DAO;

import examendeprogramacion2019.ENTITIS.Recetas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Fabian
 */
public class RecetasDAO {
    public LinkedList<Recetas> cargarTodo(String buscar, boolean todos, boolean inactivos) {
        LinkedList<Recetas> servicios = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select id, id_categoria, nombre, ingredientes, indicaciones, activo "
                    + " from Recetas ";

            sql += !todos ? " where ( lower(nombre) like lower(?))" : "";
            sql += !todos && !inactivos ? " and activo = true " : "";
            sql += todos && !inactivos ? " where activo = true" : "";

            PreparedStatement ps = con.prepareStatement(sql);
            if (!todos) {
                ps.setString(1, "%" + buscar + "%");
                ps.setString(2, "%" + buscar + "%");
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                servicios.add(cargarRecetas(rs));
            }

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
        return servicios;
    }
    private Recetas cargarRecetas(ResultSet rs) throws SQLException {
        Recetas ser = new Recetas();
        ser.setId_categoria(rs.getInt("id_categorias"));
        ser.setNombre(rs.getString("nombre"));
        ser.setIngredientes(rs.getString("ingredientes"));
        ser.setIndicaciones(rs.getString("indicaiones"));
        ser.setActivo(rs.getBoolean("activo"));
        return ser;
    }

    public void insertar(Recetas rc) {
         try ( Connection c = Conexion.conexion()) {
            String sql = "INSERT INTO Recetas(id, id_categorias, nombre, ingredientes,indicaciones,"
                    + "activo) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = null;
            stmt.setInt(1,rc.getId_categoria());
            stmt.setString(2, rc.getNombre());
            stmt.setString(3, rc.getIngredientes());
            stmt.setString(4, rc.getIndicaciones());
            stmt.setBoolean(5, rc.isActivo());
            stmt.executeUpdate();
         }catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        
    
}
    }
}
